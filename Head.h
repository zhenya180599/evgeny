#pragma once
#ifndef Furniture_h
#define Furniture_h

#include <string>
#include <iostream>
#include <vector>
#include <map>

class Furniture {
public:
	virtual std::string toString() = 0;
	void setAvailable(int count) { available = count; };

	int getAvailable() {
		return available;
	}
	float getPrice()
	{
		return Price;
	}

protected:
	float Price;
	int NumberOfParts{ 10 };
	int available;
};

class BathroomFurniture : public Furniture
{
public:
	BathroomFurniture()
	{
		Price = 100;
		available = 10;
	}

	std::string toString() override;
};

class LivingRoomFurniture : public Furniture
{
public:
	LivingRoomFurniture()
	{
		Price = 200;
		available = 5;
	}

	std::string toString() override;
};

class KitchenRoomFurniture : public Furniture
{
public:
	KitchenRoomFurniture()
	{
		Price = 300;
		available = 7;
	}

	std::string toString() override;
};

class Purchase
{
	std::vector<Furniture*> basket;

	int availableKitchenRoomFurniture;
	int availableLivingRoomFurniture;
	int availableBathroomFurniture;

public:
	float calcCost();
	void createBasket(int countK, int countL, int countB);
	void AddFurniture2Basket(Furniture* furniture, int& available, int wantToBuy);
	void setAvailableItems(int countK, int countL, int countB);
};

class Shop
{
	KitchenRoomFurniture KRF;
	LivingRoomFurniture LRF;
	BathroomFurniture BF;

	Purchase UserPurchase;

public:
	Shop();

	void changeCount(Furniture*, int);
	void startBuy();
	void prepare();
};

#endif /* Furniture_h */

