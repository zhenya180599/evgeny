//#include "stdafx.h"
#include "Header.h"
#include <string>
#include <iostream>
#include <vector>
#include <ctime>


void Unit::toString()
{
	std::cout << "AttackValue : " << this->AttackValue << std::endl;
	std::cout << "HitPoints : " << this->HitPoints << std::endl;
	std::cout << "DefenceBonus : " << this->DefenceBonus << std::endl;
	std::cout << "Cost : " << this->Cost << std::endl;
}

void Unit::SetArmyBonuses(int AttackValueBonus, int HitPointsBonus, int DefenceBonusBonus, int CostBonus)
{
	this->AttackValue += AttackValueBonus;
	this->HitPoints += HitPointsBonus;
	this->DefenceBonus += DefenceBonusBonus;
	this->Cost += CostBonus;
}

void Tank::toString()
{
	std::cout << "This is Tank params for this country:" << std::endl;
	Unit::toString();
}

void Helicopter::toString()
{
	std::cout << "This is Helicopter params for this country:" << std::endl;
	Unit::toString();
}

void Soldier::toString()
{
	std::cout << "This is Soldier params for this country:" << std::endl;
	Unit::toString();
}

void Army::toString()
{
	std::cout << "This Army has next units:" << std::endl;

	for (auto unit : Units)
	{
		switch (unit.first)
		{
		case Unit::TANK:
		{
			std::cout << "Tank : " << std::endl;
			tank.toString();
			break;
		}
		case Unit::HELICOPTER:
		{
			std::cout << "Helicopter : " << std::endl;
			helicopter.toString();
			break;
		}
		case Unit::SOLDIER:
		{
			std::cout << "Soldier : ";
			soldier.toString();
			break;
		}
		}

		std::cout << " count : " << unit.second << std::endl << std::endl;
	}
}

void RussianArmy::toString()
{
	std::cout << "This is Russian army." << std::endl;
	Army::toString();
}

void AmericanArmy::toString()
{
	std::cout << "This is American army." << std::endl;
	Army::toString();
}

bool Army::addUnit(Unit* unit)
{
	if (unit->Cost > Budget)
		return false;

	Units[unit->getType()]++;

	SumHealth += unit->HitPoints;
	SumAttack += unit->AttackValue;
	SumDefence += unit->DefenceBonus;
	SumCost += unit->Cost;
	Budget -= unit->Cost;

	return true;
}

int Army::addUnits(Unit* unit, int count)
{
	int i = 0;

	for (i = 1; i <= count; i++)
	{
		if (!addUnit(unit))
			return i - 1;
	}

	return i - 1;
}

void Army::protect(Army* enemyArmy)
{
	SumHealth -= (enemyArmy->SumAttack - SumDefence);
}

void Army::attack(Army* enemyArmy)
{
	enemyArmy->SumHealth -= (SumAttack - enemyArmy->SumDefence);
}

void Army::generateArmyRandom()
{
	std::srand(std::time(nullptr));

	addUnits(&tank, rand() % 20 + 1);
	addUnits(&helicopter, rand() % 10 + 1);
	addUnits(&soldier, rand() % 100 + 1);
}

void Army::generateArmyCustom()
{
	std::cout << "You have " << Budget << "$" << std::endl;

	int wantCount = 0;
	int addedCount = 0;

	std::cout << "Tank Cost is : " << tank.Cost << ". How Many Tanks you want?" << std::endl;
	std::cin >> wantCount;

	addedCount = addUnits(&tank, wantCount);

	std::cout << addedCount << " Tanks has been added to your army" << std::endl;
	std::cout << "You have " << Budget << "$" << std::endl << std::endl;

	std::cout << "Helicopter Cost is : " << helicopter.Cost << ". How Many Helicopters you want?" << std::endl;
	std::cin >> wantCount;

	addedCount = addUnits(&helicopter, wantCount);

	std::cout << addedCount << " Tanks has been added to your army" << std::endl;
	std::cout << "You have " << Budget << "$" << std::endl << std::endl;

	std::cout << "Soldier Cost is : " << soldier.Cost << ". How Many Soldiers you want?" << std::endl;
	std::cin >> wantCount;

	addedCount = addUnits(&soldier, wantCount);

	std::cout << addedCount << " Soldiers has been added to your army" << std::endl;
	std::cout << "You have " << Budget << "$" << std::endl << std::endl;

}

void ModernStrategy::prepareGame()
{
	std::cout << "Choose your country: Russia[1] ? America[2]" << std::endl;

	int country;
	std::cin >> country;

	switch (country)
	{
	case 1:
	{
		MyArmy = new RussianArmy();
		EnemyArmy = new AmericanArmy();

		break;
	}

	case 2:
	{
		MyArmy = new AmericanArmy();
		EnemyArmy = new RussianArmy();

		break;
	}
	}

	MyArmy->generateArmyCustom();
	EnemyArmy->generateArmyRandom();
}

void ModernStrategy::start()
{
	prepareGame();

	std::cout << std::endl;

	std::cout << "It is Your Army: " << std::endl;
	MyArmy->toString();

	std::cout << "It is Your Enemy Army: " << std::endl;
	EnemyArmy->toString();

	std::cout << std::endl;

	std::cout << "You Attack Enemy !!!" << std::endl;
	MyArmy->attack(EnemyArmy);

	std::cout << std::endl;

	std::cout << "Results: " << std::endl;

	std::cout << "It is Your Army health: " << MyArmy->SumHealth << std::endl;
	std::cout << "It is Your Enemy Army health: " << EnemyArmy->SumHealth << std::endl;

	std::cout << std::endl;

	if (MyArmy->SumHealth > EnemyArmy->SumHealth)
	{
		std::cout << "You WIN !!! :)" << std::endl;
	}
	else
	{
		std::cout << "You LOSS !!! :( " << std::endl;
	}

}


int main(int argc, const char * argv[]) {

	//lab3
	std::cout << "Welcome to new modern Strategy game!" << std::endl;

	ModernStrategy strategy;

	strategy.start();

	return 0;
}
