#pragma once


#include <map>

class Army;

class Unit
{
public:
	enum TYPE
	{
		TANK,
		HELICOPTER,
		SOLDIER
	};
	virtual void toString();
	void SetArmyBonuses(int AttackValueBonus, int HitPointsBonus, int DefenceBonusBonus, int CostBonus);

	TYPE getType()
	{
		return Type;
	}

	int AttackValue;
	int HitPoints;
	int DefenceBonus;
	int Cost;

protected:

	TYPE Type;
};

class Tank : public Unit
{
public:
	void toString() override;

	Tank()
	{
		this->AttackValue = 25;
		this->HitPoints = 100;
		this->DefenceBonus = 30;
		this->Cost = 150;
		Type = TANK;
	}
};

class Helicopter : public Unit
{
public:
	void toString() override;

	Helicopter()
	{
		this->AttackValue = 40;
		this->HitPoints = 50;
		this->DefenceBonus = 15;
		this->Cost = 250;
		Type = HELICOPTER;
	}
};

class Soldier : public Unit
{
public:
	void toString() override;

	Soldier()
	{
		this->AttackValue = 5;
		this->HitPoints = 10;
		this->DefenceBonus = 3;
		this->Cost = 20;
		Type = SOLDIER;
	}
};

class Army {
public:
	virtual void toString();
	virtual void protect(Army*);
	virtual void attack(Army*);

	virtual void generateArmyRandom();
	virtual void generateArmyCustom();

	bool addUnit(Unit*);
	int addUnits(Unit*, int);

public:
	int SumHealth;
	int SumAttack;
	int SumDefence;
	int SumCost;
	int Budget{ 5000 };

protected:
	int AttackValueBonus;
	int HitPointsBonus;
	int DefenceBonusBonus;
	int CostBonus;

	std::map<Unit::TYPE, int> Units;

	Soldier soldier;
	Helicopter helicopter;
	Tank tank;

};

class RussianArmy : public Army {
public:
	RussianArmy()
	{
		this->AttackValueBonus = 5;
		this->HitPointsBonus = -5;
		this->DefenceBonusBonus = 10;
		this->CostBonus = -5;

		soldier = Soldier();
		helicopter = Helicopter();
		tank = Tank();

		soldier.SetArmyBonuses(AttackValueBonus, HitPointsBonus, DefenceBonusBonus, CostBonus);
		helicopter.SetArmyBonuses(AttackValueBonus, HitPointsBonus, DefenceBonusBonus, CostBonus);
		tank.SetArmyBonuses(AttackValueBonus, HitPointsBonus, DefenceBonusBonus, CostBonus);
	}

	void toString() override;
};

class AmericanArmy : public Army {
public:
	AmericanArmy()
	{
		this->AttackValueBonus = -5;
		this->HitPointsBonus = 5;
		this->DefenceBonusBonus = 5;
		this->CostBonus = 5;

		soldier = Soldier();
		helicopter = Helicopter();
		tank = Tank();

		soldier.SetArmyBonuses(AttackValueBonus, HitPointsBonus, DefenceBonusBonus, CostBonus);
		helicopter.SetArmyBonuses(AttackValueBonus, HitPointsBonus, DefenceBonusBonus, CostBonus);
		tank.SetArmyBonuses(AttackValueBonus, HitPointsBonus, DefenceBonusBonus, CostBonus);
	}

	void toString() override;
};

class ModernStrategy {

public:
	void start();

private:
	void prepareGame();

	Army* MyArmy;
	Army* EnemyArmy;
};
