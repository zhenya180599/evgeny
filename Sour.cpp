//#include "stdafx.h"
#include <iostream>
#include "Head.h"

Shop::Shop()
{
	KitchenRoomFurniture KRF;
	LivingRoomFurniture LRF;
	BathroomFurniture BF;

	KRF.setAvailable(10);
	LRF.setAvailable(7);
	BF.setAvailable(35);
}

std::string BathroomFurniture::toString()
{
	return "BathroomFurniture";
}

std::string LivingRoomFurniture::toString()
{
	return "LivingRoomFurniture";
}

std::string KitchenRoomFurniture::toString()
{
	return "KitchenRoomFurniture";
}

void Purchase::AddFurniture2Basket(Furniture* furniture, int& available, int wantToBuy)
{
	int count = 0;
	for (int i = 0; i < wantToBuy; i++)
	{
		if (available > 0)
		{
			basket.push_back(furniture);
			count++;
			available--;
		}
	}

	std::cout << count << " : (" << available << " left in Shop) of " << furniture->toString() << "has been added to your Basket" << std::endl;
}

void Purchase::setAvailableItems(int countK, int countL, int countB)
{
	availableKitchenRoomFurniture = countK;
	availableLivingRoomFurniture = countL;
	availableBathroomFurniture = countB;
}

float Purchase::calcCost()
{
	float sum = 0;

	for (auto furniture : basket)
	{
		sum += furniture->getPrice();
	}

	return sum;
}

void Purchase::createBasket(int countK, int countL, int countB)
{
	AddFurniture2Basket(new KitchenRoomFurniture(), availableKitchenRoomFurniture, countK);
	AddFurniture2Basket(new LivingRoomFurniture(), availableLivingRoomFurniture, countL);
	AddFurniture2Basket(new BathroomFurniture(), availableBathroomFurniture, countB);
}

void Shop::prepare()
{
	int KRFCount = 0;
	int LRFCount = 0;
	int BFACount = 0;

	std::cout << "Insert available count of KitchenRoomFurniture LivingRoomFurniture BathroomFurniture : " << std::endl;
	std::cin >> KRFCount >> LRFCount >> BFACount;

	UserPurchase.setAvailableItems(KRFCount, LRFCount, BFACount);
}

void Shop::startBuy()
{
	int KRFCount = 0;
	int LRFCount = 0;
	int BFACount = 0;

	std::cout << "Insert want to buy count of KitchenRoomFurniture LivingRoomFurniture BathroomFurniture : " << std::endl;
	std::cin >> KRFCount >> LRFCount >> BFACount;

	UserPurchase.createBasket(KRFCount, LRFCount, BFACount);

	std::cout << "Calculated cost " << UserPurchase.calcCost() << std::endl;

}

int main(int argc, const char * argv[]) {

	//lab3  task 2
	std::cout << "Welcome to our shop!" << std::endl;

	Shop myShop;

	myShop.prepare();
	myShop.startBuy();

	return 0;
}
